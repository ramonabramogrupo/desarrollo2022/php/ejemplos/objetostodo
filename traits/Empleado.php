<?php

class Empleado extends Base {

    use Direccion;

    public $escala;

    public function __construct($escala) {
        $this->escala = $escala;
        parent::__construct();
    }

    public function getEscala() {
        return $this->escala;
    }

    public function setEscala($escala) {
        $this->escala = $escala;
        return $this;
    }

    public function presentacion() {
        return "{$this->nombre},{$this->apellidos},{$this->fechaNacimiento},{$this->escala},{$this->calcularSueldo()}";
    }

    public function calcularSueldo() {
        switch (true) {
            case ($this->escala < 10):
                return 800;
            case ($this->escala < 50):
                return 1000;
            case ($this->escala < 100):
                return 1200;
            default :
                return 1500;
        }
    }

    public function mostrarInformacion() {
        foreach ($this as $nombre => $valor) {
            echo "{$nombre}:{$valor}";
        }
    }

}
