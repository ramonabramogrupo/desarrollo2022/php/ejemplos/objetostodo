<?php

trait Direccion {

    public $direccion;
    public $tipo;
    public $numero;
    public $bloque;
    public $puerta;
    public $poblacion;
    public $cp;

    public function __construct($direccion, $tipo, $numero, $bloque, $puerta, $poblacion, $cp) {
        $this->direccion = $direccion;
        $this->tipo = $tipo;
        $this->numero = $numero;
        $this->bloque = $bloque;
        $this->puerta = $puerta;
        $this->poblacion = $poblacion;
        $this->cp = $cp;
    }

    public function getDireccion() {
        return $this->direccion;
    }

    public function getTipo() {
        return $this->tipo;
    }

    public function getNumero() {
        return $this->numero;
    }

    public function getBloque() {
        return $this->bloque;
    }

    public function getPuerta() {
        return $this->puerta;
    }

    public function getPoblacion() {
        return $this->poblacion;
    }

    public function getCp() {
        return $this->cp;
    }

    public function setDireccion($direccion) {
        $this->direccion = $direccion;
        return $this;
    }

    public function setTipo($tipo) {
        $this->tipo = $tipo;
        return $this;
    }

    public function setNumero($numero) {
        $this->numero = $numero;
        return $this;
    }

    public function setBloque($bloque) {
        $this->bloque = $bloque;
        return $this;
    }

    public function setPuerta($puerta) {
        $this->puerta = $puerta;
        return $this;
    }

    public function setPoblacion($poblacion) {
        $this->poblacion = $poblacion;
        return $this;
    }

    public function setCp($cp) {
        $this->cp = $cp;
        return $this;
    }

}
