<?php

spl_autoload_register(function ($clase) {
    require "clases/{$clase}.php";
});

// $a = new Ser(); // no se puede instanciar una clase abstracta

$a = new Humano();
var_dump($a);
