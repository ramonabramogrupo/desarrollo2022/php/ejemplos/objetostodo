<?php

class Humano extends Ser {

    private string $nombre;
    private int $edad;

    public function despedir(): string {
        return "adios";
    }

    public function saludar(): string {
        return "hola";
    }

    public function __construct(string $nombre, int $edad) {
        $this->nombre = $nombre;
        $this->edad = $edad;
        $this->tipo = "ser humano";
    }

}
