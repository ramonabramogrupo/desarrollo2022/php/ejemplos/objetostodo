<?php

abstract class Ser {

    private string $tipo;

    public function getTipo(): string {
        return $this->tipo;
    }

    public function setTipo(string $tipo): void {
        $this->tipo = $tipo;
    }

    abstract public function saludar(): string; // la subclase tiene que sobreescribir este metodo

    abstract public function despedir(): string; // la subclase tiene que sobreescribir este metodo
}
