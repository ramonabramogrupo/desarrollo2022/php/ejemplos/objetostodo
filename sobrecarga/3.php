<?php

spl_autoload_register(function ($clase) {
    require "clases/{$clase}.php";
});

// $jose = new Persona(); //da error no admite varios constructores

$jose = new Persona3(); // se llama al primer constructor
var_dump($jose);
$ana = new Persona3("ana gomez"); // se llama al segundo constructor
var_dump($ana);
$luisa = new Persona3("Luisa Perez", "M"); // se llama al tercer constructor
var_dump($luisa);
$eva = new Persona3("Eva Saiz", "M", 34); // se llama al cuarto constructor
var_dump($eva);

