<?php

/**
 * Description of Persona
 *
 * @author ramon
 */
class Persona {

    public ?string $nombre = null;
    public string $sexo = 'H';
    public int $edad = 0;

    /**
     *
     * quiero conseguir que la clase pueda tener 4 constructores
     * php asi no lo permite
     */
    public function __construct(?string $nombre, string $sexo, int $edad) {
        $this->nombre = $nombre;
        $this->sexo = $sexo;
        $this->edad = $edad;
    }

    public function __construct(?string $nombre, string $sexo) {
        $this->nombre = $nombre;
        $this->sexo = $genero;
    }

    public function __construct(?string $nombre) {
        $this->nombre = $nombre;
    }

    public function __construct() {

    }

}
