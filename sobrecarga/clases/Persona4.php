<?php

/**
 * Description of Persona4
 *
 * @author ramon
 */
class Persona4 {

    public ?string $nombre = null;
    public string $sexo = 'H';
    public int $edad = 0;

    /**
     *
     * quiero conseguir que la clase pueda tener 4 metodos saludar
     *
     */
    public function __call($name, $datos) {
        if ($name == "saludar") {
            $numero = count($datos); // contamos cuantos argumentos he pasado al metodo
            $nombre = "{$name}{$numero}"; // creo el nombre del metodo en funcion del numero de argumentos pasados
            if (method_exists($this, $nombre)) {
                //return $this->$nombre(...$datos); // llamo al metodo con una funcion variable
                return call_user_func_array([$this, $nombre], $datos); // llamo al metodo con la funcion call
            }
        }
    }

    public function saludar1($a) {
        return "Hola {$a}";
    }

    public function saludar2($a, $b) {
        return "Hola {$a} Adios {$b}";
    }

    public function saludar3($a, $b, $c) {
        return "Hola {$a} Adios {$b} Bienvenido {$c}";
    }

}
