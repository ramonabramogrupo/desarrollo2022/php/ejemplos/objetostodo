<?php

/**
 * Description of Persona
 *
 * @author ramon
 */
class Persona5 {

    public ?string $nombre = null;
    public string $sexo = 'H';
    public int $edad = 0;

    public function __construct(array $datos) {
        $inicio = [
            "nombre" => $this->nombre,
            "sexo" => $this->sexo,
            "edad" => $this->edad
        ]; // creo un array con los valores por defecto de las propiedades

        $d = array_intersect_key($datos, $inicio); // me quedo con los indices que me hayan pasado al constructor que existan como propiedad
        $inicio = array_replace($inicio, $d); // el array inicio se reemplaza con los valores pasados al constructor

        foreach ($inicio as $propiedad => $valor) {
            $this->$propiedad = $valor;
        }
    }

}
