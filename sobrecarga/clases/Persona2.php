<?php

/**
 * Description of Persona2
 *
 * @author ramon
 */
class Persona2 {

    public ?string $nombre = null;
    public string $sexo = 'H';
    public int $edad = 0;

    /**
     *
     * quiero conseguir que la clase pueda tener 4 constructores
     *
     */
    public function __construct(...$datos) {
        // en el argumento datos tenemos un array con todo lo enviado
        $numero = count($datos); // contamos cuantos argumentos he pasado al constructor
        $nombre = "constructor{$numero}"; // creo el nombre del constructor en funcion del numero de argumentos pasados

        if (method_exists($this, $nombre)) {
            $this->$nombre(...$datos); // llamo al metodo constructor con una funcion variable
        }
    }

    private function constructor0() {

    }

    private function constructor1($nombre) {

        $this->nombre = $nombre;
    }

    private function constructor2($nombre, $sexo) {

        $this->nombre = $nombre;
        $this->sexo = $sexo;
    }

    private function constructor3($nombre, $sexo, $edad) {

        $this->nombre = $nombre;
        $this->sexo = $sexo;
        $this->edad = $edad;
    }

}
