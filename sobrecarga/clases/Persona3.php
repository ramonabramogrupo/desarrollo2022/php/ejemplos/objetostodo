<?php

/**
 * Description of Persona2
 *
 * @author ramon
 */
class Persona3 {

    public ?string $nombre = null;
    public string $sexo = 'H';
    public int $edad = 0;

    /**
     *
     * quiero conseguir que la clase pueda tener 4 constructores
     *
     */
    public function __construct(?string $nombre = null, string $sexo = 'H', int $edad = 0) {
        $this->nombre = $nombre;
        $this->sexo = $sexo;
        $this->edad = $edad;
    }

}
