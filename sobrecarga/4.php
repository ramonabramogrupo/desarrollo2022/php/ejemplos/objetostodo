<?php

spl_autoload_register(function ($clase) {
    require "clases/{$clase}.php";
});

// $jose = new Persona(); //da error no admite varios constructores

$jose = new Persona4(); // se llama al primer constructor
echo $jose->saludar("ramon");
echo $jose->saludar("ramon", "ana");
echo $jose->saludar("ramon", "ana", "luis");
