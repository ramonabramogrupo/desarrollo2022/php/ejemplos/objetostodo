<?php

class Usuario1 implements Todo {

    public function Hola(): string {
        return "Hola";
    }

    public function Adios(): string {
        return "Adios";
    }

    public function calcularSueldo() {
        return 1000;
    }

    public function mostrarInformacion() {
        return "informacion";
    }

    public function salir(): string {
        return "saliendo";
    }

}
