<?php

interface Trabajador {

    public function calcularSueldo();

    public function mostrarInformacion();
}
