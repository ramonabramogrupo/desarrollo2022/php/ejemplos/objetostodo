<?php

class Usuario implements Persona, Trabajador {

    public function hola(): string {
        return "Hola";
    }

    public function adios(): string {
        return "Adios";
    }

    public function calcularSueldo() {
        return 1000;
    }

    public function mostrarInformacion() {
        return "informacion";
    }

}
