<?php

interface Persona {

    public function hola(): string;

    public function adios(): string;
}
